window.App = window.App || {};

App.Views = {};

_.templateSettings = {
    interpolate: /\{\{(.+?)\}\}/g
};

App.Views.Month = Backbone.View.extend({
    template: _.template($('#month-template').html()),
    initialize: function () {
        this.currentDate = this.model.moment();
        this.model.on('change', this.render, this);
    },
    events: {
        'click .prev': 'goPrev',
        'click .next': 'goNext'
    },
    goPrev: function () {
        this.currentDate.subtract(1, 'month');
        this.model.set({
            'month': this.currentDate.get('month'),
            'year': this.currentDate.get('year'),
            'name': this.currentDate.format('MMMM'),
            'days': this.currentDate.daysInMonth(),
            'weeks': Math.ceil((this.model.get('days') + this.currentDate.day())/7)
        });
    },
    goNext: function () {
        this.currentDate.add(1, 'month');
        this.model.set({
            'month': this.currentDate.get('month'),
            'year': this.currentDate.get('year'),
            'name': this.currentDate.format('MMMM'),
            'days': this.currentDate.daysInMonth(),
            'weeks': Math.ceil((this.model.get('days') + this.currentDate.day())/7)
        })
    },
    render: function () {

        this.el.innerHTML = this.template(this.model.toJSON());
        var weeks = this.model.get('weeks');

        for (var i = 0; i < weeks; i++) {

            var weekRow = new App.Views.WeekRow({
                week  : i,
                model : this.model,
                collection: this.collection
            }).render().el;

            this.$('tbody').append(weekRow);
        }
        return this;
    }
});

App.Views.WeekRow = Backbone.View.extend({
    tagName: 'tr',
    initialize: function (options) {
        if (options) {
            this.week = options.week;
        }
    },
    render: function () {
        var month = this.model;

        if (this.week === 0) {
            var firstDay = month.moment().day();
            for (var i = 0; i < firstDay; i++) {
                this.$el.append('<td></td>');
            }
        }

        month.weekDates(this.week).forEach(function (date) {
            //dateTest = month.moment().date(date);
            //debugger;
            this.$el.append('<td>' + date + '</td>');
        }, this);

        return this;
    }
});


//App.Views.Day = Backbone.View.extend({
//    template: '<div class="date"><{{num}}</div>',
//    initialize: function (options) {
//        this.date = options.date;
//    },
//    render: function () {
//        this.el.innerHTML = this.template({
//            date: this.date.format("MMMM D, YYYY")
//        });
//        this.$('div').append(new App.Views.DayTable({
//            date: this.date,
//            collection: this.collection
//        }).render().el);
//
//        this.$('div').append('<div>');
//
//
//        return this;
//    }
//});
